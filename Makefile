install.reviewdog:
	wget -O - -q https://raw.githubusercontent.com/reviewdog/reviewdog/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

lint:
	golangci-lint run --out-format=line-number ./... | reviewdog -f=golangci-lint -reporter=bitbucket-code-report

run.docker:
	docker-compose up -d

install.docker-compose:
	curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose

install.migrate:
	go install -tags 'neo4j' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

migrate:
	migrate -source file:migrations/ -database neo4j://neo4j:test@localhost:7687/database up

install.reviewdog:
	wget -O - -q https://raw.githubusercontent.com/reviewdog/reviewdog/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

lint:
	golangci-lint run --out-format=line-number ./... | reviewdog -f=golangci-lint -reporter=bitbucket-code-report

install.docker-compose:
	curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose

run.docker:
	docker-compose up -d

install.migrate:
	go install -tags 'neo4j' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

migrate:
	migrate -source file:migrations/ -database neo4j://neo4j:test@localhost:7687/database up


build:
	go build -o bin/main main.go

test:
	go test neo4j/* -v

run:
	go run main.go
