package neo4j

// Config set of neo4j configurations

type Config struct {
	URI      string `required:"neo4j://localhost.com:7687"`
	User     string `required:"neo4j"`
	Password string `required:"test"`
}
